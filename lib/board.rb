#Anastassia Bobokalonova
#April 16, 2017

class Board

  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    # unless empty?(pos)
    #   puts "This spot is already filled!"
    # end
    grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    spot = grid[pos[0]][pos[1]]
    spot ? false : true
  end

  def winner
    row1 = [grid[0][0], grid[0][1], grid[0][2]]
    row2 = [grid[1][0], grid[1][1], grid[1][2]]
    row3 = [grid[2][0], grid[2][1], grid[2][2]]
    column1 = [grid[0][0], grid[1][0], grid[2][0]]
    column2 = [grid[0][1], grid[1][1], grid[2][1]]
    column3 = [grid[0][2], grid[1][2], grid[2][2]]
    left_d = [grid[0][0], grid[1][1], grid[2][2]]
    right_d = [grid[0][2], grid[1][1], grid[2][0]]
    all_positions = [row1, row2, row3, column1, column2, column3, left_d, right_d]

    all_positions.each do |pos|
      return :X if pos == [:X, :X, :X]
      return :O if pos == [:O, :O, :O]
    end
    nil
  end

  def over?
    return true if winner
    grid.each do |row|
      row.each do |pos|
        return false if pos == nil
      end
    end
    true
  end

  def write(board = self)
    grid.each {|row| p row}
  end

end
