#Anastassia Bobokalonova
#April 16, 2017

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
=begin
In your `Game` class, set the marks of the players you are passed. Include
  the following methods:
  - `current_player`
  - `switch_players!`
  - `play_turn`, which handles the logic for a single turn
  - `play`, which calls `play_turn` each time through a loop until the game is
    over
=end

class Game
  attr_reader :player_one, :player_two
  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
  end

  def current_player
    players = [player_one, player_two]
    @current_player = players[0]
  end

  def switch_players!

  end

  def play_turn
    current_player.get_move
  end

  def play
    play_turn
  end


end
