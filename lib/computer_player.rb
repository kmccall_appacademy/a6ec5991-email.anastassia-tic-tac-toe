#Anastassia Bobokalonova
#April 16, 2017

class ComputerPlayer
  attr_reader :name
  attr_accessor :board, :mark

  def initialize(name)
    @mark = :O
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    board.write
    return winning_move if winning_move
    random_move
  end

  def winning_move
    test_board = Board.new
    test_board = copy_board(test_board)
    spots = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]

    spots.each do |pos|
      if test_board.empty?(pos)
        test_board.place_mark(pos, mark)
        return pos if test_board.winner
        test_board.place_mark(pos, nil)
      end
    end

    nil
  end

  def random_move
    x = rand(2)
    y = rand(2)
    spot = board.grid[x][y]
    
    if spot == nil
      return [x, y]
    else
      return random_move
    end

  end

  def copy_board(new_board)
    spots = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]
    spots.each do |pos|
      old_mark = board.grid[pos[0]][pos[1]]
      if old_mark
        new_board.place_mark(pos, old_mark)
      end
    end
    new_board
  end

end
