#Anastassia Bobokalonova
#April 16, 2017

class HumanPlayer
  attr_reader :mark, :name

  def initialize(name)
    @mark = :X
    @name = name
  end

  def get_move
    puts "Where?"
    move = gets.chomp
    pos = move.split(", ")
    pos.map! {|n| n.to_i}
  end

  def diplay(board)
    board.write
  end


end
